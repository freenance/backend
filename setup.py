from setuptools import setup

with open("requirements.txt") as f:
    requirements = f.readlines()

setup(
    name='freenance',
    packages=['freenance'],
    include_package_data=True,
    install_requires=requirements,
)