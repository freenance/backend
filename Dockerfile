FROM python:latest
RUN python3 -m pip install -U pip
#RUN pip install -U duckduckgo_search
#COPY requirements.txt .
#RUN pip install -r requirements.txt
 
WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
RUN set -a && . ./.env
CMD ["bash", "-c", "set -a && . .env && flask run"]

