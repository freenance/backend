# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import re
import json
from datetime import datetime

PATH = str("scripts/כל הכרטיסים - חיובים בשקלים.csv")

def find_columns(data):
    for i in range(len(data)):
        match = re.search("\d{2}[\./]\d{2}[\./]\d{4}", data[i])
        if match:
            break            
    check = data[i].split(',')
    dates = []
    for a in range(len(check)):
        match = re.search("\d{2}[\./]\d{2}[\./]\d{4}", check[a])
        if match:
            check[a] = datetime.strptime(check[a], '%d/%m/%Y')
            dates.append([check[a], a])
    
    if dates[0][0] > dates[1][0]:
        date_column = dates[1][1]
    else:
        date_column = dates[0][1]
        
    second_column = max(dates[0][1], dates[1][1]) + 1
    # in the future - second column should be the first to contain text that is not  date/number

    for i in range(len(data)):
        match = re.search(str(r".*סכום.*"), data[i])
        if match:
            break
    check1 = data[i].split(',')
    sums = []
    for a in range(len(check1)):
        match = re.search(str(r".*סכום.*"), check1[a])
        if match:
            sums.append(a)
    print(sums)
    if check[sums[0]] > check[sums[1]]:
        third_column = sums[0]
    else:
        third_column = sums[1]
    return date_column, second_column, third_column
    

def create_new_table(data, indexes):
    new_data = []
    for line in data:
        lst = line.split(',')
        new_data.append([lst[indexes[0]], lst[indexes[1]], lst[indexes[2]]])
    return new_data
                          

def main():
    with open(PATH, mode='r', encoding="utf-8") as f:
        data = f.readlines()
    indexes = find_columns(data)
    final = create_new_table(data, indexes)
    print(json.dumps(final))
    

main()
