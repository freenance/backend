import random

from requests.sessions import Session
from lxml import html
from torpy.http.requests import tor_requests_session

__version__ = 0.4

user_agents = [
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0",
]


def ddg(keywords, region='he-il', safesearch='Moderate', time=None, max_results=30, retries=5,use_tor=False, **kwargs):
    '''
    DuckDuckGo search
    Query parameters, link: https://duckduckgo.com/params:
    keywords: keywords for query;
    safesearch: On (kp = 1), Moderate (kp = -1), Off (kp = -2);
    region: country of results - wt-wt (Global), us-en, uk-en, ru-ru, etc.;
    time: 'd' (day), 'w' (week), 'm' (month), 'y' (year), or 'year-month-date..year-month-date';
    max_results: depends on the keyword, the maximum DDG gives out about 200 results.
    '''
    
    for _ in range(retries):
        try:
            results, counter = [], 0
            url = 'https://html.duckduckgo.com/html'
            safesearch_base = {'On': 1, 'Moderate': -1, 'Off': -2}
            payload = {'q': keywords, 'l': region,
                       'p': safesearch_base[safesearch], 'df': time}
            headers = {"User-Agent": random.choice(user_agents)}

            while True:
                session = Session()
                if use_tor:
                    session = tor_requests_session(2)
                with session as s:
                    res = s.post(url, headers=headers, data=payload)
                    tree = html.fromstring(res.text)
                    if tree.xpath('//div[@class="no-results"]/text()'):
                        return results
                    for element in tree.xpath('//div[contains(@class, "results_links")]'):
                        position = {
                            'title': element.xpath('.//a[contains(@class, "result__a")]/text()')[0],
                            'href': element.xpath('.//a[contains(@class, "result__a")]/@href')[0],
                            'body': ''.join(element.xpath('.//a[contains(@class, "result__snippet")]//text()')),
                        }
                        results.append(position)
                        counter += 1
                        if counter >= max_results:
                            return results

                    next_page = tree.xpath('.//div[@class="nav-link"]')[-1]
                    names = next_page.xpath('.//input[@type="hidden"]/@name')
                    values = next_page.xpath('.//input[@type="hidden"]/@value')
                    payload = {n: v for n, v in zip(names, values)}

        except Exception as e:
            use_tor = True
            print(e)
    return [{"body":"", "title":"", "href":"https://www.google.com"}]
