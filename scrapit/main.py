#from duckduckgo_search import ddg
from scrapit.duckduckgo_search import ddg
from functools import lru_cache
import typing
import re
import sys
import json
import requests

from torpy.http.requests import tor_requests_session


cached_ddg = lru_cache(ddg)
url_blacklists = ["wikipedia"]

good_words = ["צור קשר", "יצירת קשר", "טלפון"]

# https://stackoverflow.com/questions/123559/how-to-validate-phone-numbers-using-regex
# phone_regex = re.compile(
#     r"/^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i"
# )
phone_regex = re.compile(
    r"(?:(?:(\+?972|\(\+?972\)|\+?\(972\))(?:\s|\.|-)?([1-9]\d?))|(0[23489]{1})|(0[57]{1}[0-9]))(?:\s|\.|-)?([^0\D]{1}\d{2}(?:\s|\.|-)?\d{4})"
)
mail_regex = re.compile(
    r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'"""
)
regexes = [
    phone_regex,
    mail_regex
]

# cat results.json | jq '.[] | select(.title==  )


def search(term):
    results = cached_ddg(term, region="he-il")
    return results


def in_title(name, result):
    return is_in(name, result["title"])

def in_body(name : typing.Union[str , typing.Pattern[str]] , result):
    return is_in(name, result["body"])
    

def is_in(substring: typing.Union[str , typing.Pattern[str]], string):
    if isinstance(substring, re.Pattern):
        return bool(re.findall(substring, string))
    return substring in string


def score_result(result, names, words):
    score = 0
    substrings = []
    substrings.extend(names)
    substrings.extend(words)
    
    for i, substring in enumerate(substrings):
        if any(b in result["href"] for b in url_blacklists):
            break
        if in_body(substring, result):
            score += 3
            score -= i*0.5
        if in_title(substring, result):
            score += 7
            score -= i*0.5
    for regex in regexes:
        if in_body(regex, result):
            score += 13
            score -= i*0.5
    return score


def score_results(argv=" ", search_results=[]):
    names = [r.strip() for r in argv.strip().split(" ") if len(r) > 1]
    out = dict()
    for i, result in enumerate(
        search_results[::-1]
    ):  # we are using the index in the calculation, because the higher result the better
        out[int(score_result(result, names, good_words)) + (i*2)] = result
    return out


def get_phone_number(site_data):
    regex_result = re.findall(phone_regex, site_data)
    if regex_result:
        return "".join(regex_result[0])
    return None

def get_email_address(site_data):
    regex_result = re.findall(mail_regex, site_data)
    if regex_result:
        return "".join(regex_result[0])
    return None

def enrich(buissness_name):
    # TODO remove this - this is very bad
    results = search(buissness_name + " צור קשר")
    # with open("results.json", "r") as f:
    #     results = json.load(f)
    scored_results = score_results(buissness_name, results)
    best_result = scored_results[max(scored_results.keys())]
    print(best_result)
    print(best_result["title"][::-1])
    try:
        best_result_site_response = requests.get(best_result["href"])
        assert best_result_site_response.ok
    except:
        return None, None, best_result["href"]
    best_result_site_page = best_result_site_response.text
    # print(best_result_site_page)
    phone_number = get_phone_number(best_result_site_page)
    if not phone_number:
        phone_number = get_phone_number(best_result["body"])
    email = get_email_address(best_result_site_page)
    if not email:
        email = get_email_address(best_result["body"])
    

    return phone_number, email, best_result["href"] 


if __name__ == "__main__":
    argv = "מקדונלד'ס רמת אביב"
    print(enrich(argv))
