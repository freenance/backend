import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # ...
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CORS_ALLOW_HEADERS = "*"
    CORS_ALWAYS_SEND = True
    CORS_AUTOMATIC_OPTIONS = True
    CORS_EXPOSE_HEADERS = True
    CORS_INTERCEPT_EXCEPTIONS = True
    CORS_MAX_AGE = None
    CORS_METHODS = ["GET", "HEAD", "POST", "OPTIONS", "PUT", "PATCH", "DELETE"]
    CORS_ORIGINS = "*"
    CORS_RESOURCES = r"/*"
    CORS_SEND_WILDCARD = True
    CORS_SUPPORTS_CREDENTIALS = True
    CORS_VARY_HEADER = True
