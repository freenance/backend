from freenance.api import register_endpoints
from freenance.models import create_db
from freenance.config import Config
from flask_cors import CORS

from flask import Flask

def create_app():
    app = Flask(__name__)
    app.config['CORS_HEADERS'] = 'Content-Type'
    CORS(app, vary_header=True, send_wildcard=True,allow_headers=True, resources={r'*': {'origins': '*'}})
    app.config.from_object(Config)
    
    @app.after_request
    def apply_caching(response):
        response.headers["Access-Control-Allow-Origin"] = "*"
        response.headers["Access-Control-Allow-Headers"] = "*"
        print(response.headers)
        return response
    with app.app_context():
        create_db()
        register_endpoints()

    return app
