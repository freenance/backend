import json
from flask import current_app, redirect, url_for, request, render_template

@current_app.route("/dd")
def lalahello():
    return "Hello there"

@current_app.route("/upload-credit", methods=['POST'])
def credit_upload():
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@current_app.route("/upload-bank", methods=['POST'])
def bank_upload():
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@current_app.route("/get-tasks")
def get_tasks():
    return """
{
	"count": 2,
	"error": null,
	"data": [
		{
			"id": 1234,
			"expense": 4444,
			"title": "disconnect from the water company",
			"shortcuts": {
				// note that some fields might be null
				"website": null,
				"email": "contact@water.co.il",
				 // TODO make number?
				"phone": "+9723-0000-000" 
			},
			"done": false
		},
		{
			"id": 53534,
			"expense": 4343,
			"title": "disconnect from the XYZ company",
			"shortcuts": {
				// note that some fields might be null
				"website": "https://abc.xyz",
				"email": "contact@xyz.co.il",
				 // TODO make number?
				"phone": "+9723-0000-000" 
			},
			"done": true
		}
	]
}
    """, 200, {'ContentType':'application/json'}

@current_app.route("/get-expenses")
def get_expenses():
    return """
{
	"count": 3,
	"error": null,
	"data": [
		{
			"id": 1234,
			"title": "AAA",
			"cost": 1337,
			"is_all_right": false,		
		},
		{
			"id": 54353,
			"title": "BBB",
			"cost": 1234,
			"is_all_right": true,		
		}
		{
			"id": 7474,
			"title": "CCC",
			"cost": 500,
			"is_all_right": false,
		}
	]
}
    """, 200, {'ContentType':'application/json'}

@current_app.route("/get-cash")
def get_cash():
    return """
    {
	"count": 6,
	"error": null,
	"data": [
		// timestamp, cost, is_all_right
		[1621752364, 500, false],
		[1601752364, 350, true],
		[1621753364, 100, false],
		[142752364, 1000, true],
		[1521752364, 30, false],
		[1221752364, 432, false],
	]
    }
    """
