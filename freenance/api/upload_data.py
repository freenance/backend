import json
import datetime
import flask
from flask import current_app, request, jsonify, abort
from flask_cors import cross_origin

from freenance.utils import db, logged_in
from freenance.main.anomaly_detector import detect_anomalies
from freenance.main.normal_expenses import detect_normal_expenses
from freenance.main.missions import get_missions

from freenance.models.user import User, Anomalies
from freenance.models.records import BankRecords, CreditTables, CreditEntries


@current_app.route("/upload-bank-records", methods=["POST", "OPTIONS"])
#@logged_in
def upload_bank_records(user=None):
    if request.method == "OPTIONS":
        return "ok", 200
    for row in request.get_json()["data"]:
        b = BankRecords(
            date=datetime.datetime.strptime(row[0], "%d/%m/%Y"),
            name=row[1],
            price=row[2],
        )
        user.bank_records.append(b)
        # TODO start a thread to get data from that to the missions and everything else
    db.session.commit()

    resp = jsonify(success=True)
    resp.status_code = 200
    return resp


@current_app.route("/upload-data", methods=["POST", "OPTIONS"])
#@logged_in
def upload_credit_card(user=None):
    if request.method == "OPTIONS":
        return "OK", 200
    j = request.get_json()
    anomalies = detect_anomalies(j["data"])
    normal_expenses = detect_normal_expenses(j["data"])
    missions = get_missions(normal_expenses, anomalies)

    resp = jsonify(
        success=True,
        normal_expenses=normal_expenses,
        anomalies=anomalies,
        missions=missions,
    )
    resp.status_code = 200
    return resp
