import json
from flask import current_app, request, jsonify, abort
from flask_cors import cross_origin

from freenance.utils import db
from freenance.models.user import User


@current_app.route("/create-user", methods=['POST', 'OPTIONS'])
def create_user():
    if request.method == 'OPTIONS':
        return "ok", 200
    new_user = User(**request.get_json())
    db.session.add(new_user)    
    try:
        db.session.commit()
    except Exception:
        resp = jsonify(success=False)
        resp.status_code = 401
        return resp

    resp = jsonify(success=True, id=new_user.id)
    resp.status_code = 200

    return resp

@current_app.route("/check-user", methods=['POST', 'OPTIONS'])
def check_user():
    if request.method == 'OPTIONS':
        return "ok", 200
    j = request.get_json()
    user = User.query.filter_by(email=j['email'], password=j['password']).first()
    if user:
        resp = jsonify(success=True, id=user.id)
        resp.status_code = 200
        return resp
    
    resp = jsonify(success=False)
    resp.status_code = 401
    return resp
