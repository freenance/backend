import json
import datetime
import flask
from flask import current_app, request, jsonify, abort
from flask_cors import cross_origin
from scrapit.main import enrich as scrapit_enrich

from freenance.utils import db, logged_in
from freenance.main.anomaly_detector import detect_anomalies
from freenance.main.normal_expenses import detect_normal_expenses
from freenance.main.missions import get_missions

from freenance.models.user import User, Anomalies
from freenance.models.records import BankRecords, CreditTables, CreditEntries

@current_app.route("/enrich", methods=["POST", "OPTIONS"])
def enrich(user=None):
    if request.method == "OPTIONS":
        return "OK", 200
    j = request.get_json()
    p = scrapit_enrich(j['name'])
    phone, email, website = p
    resp = jsonify(
        success=True,
        phone=phone,
        email=email,
        website=website, 
    )
    resp.status_code = 200
    return resp
