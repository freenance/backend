from freenance.utils import db

def create_db():
    from freenance.models.user import User
    from freenance.models.records import BankRecords
    db.create_all()
    db.session.commit()
    