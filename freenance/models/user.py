from freenance.utils.database import db

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True)
    user_name = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(128), nullable=False)
    anomalies = db.relationship("Anomalies")
    bank_records = db.relationship("BankRecords")
    credit_card_tables = db.relationship("CreditTables")

    def __repr__(self):
        return '<User {}>'.format(self.email)


class Anomalies(db.Model):
    __tablename__ = 'anomalies'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(1000), nullable=False)
    price = db.Column(db.Float, nullable=False)
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))
