from freenance.utils.database import db

class BankRecords(db.Model):
    __tablename__ = 'bank_records'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(1000), nullable=False)
    price = db.Column(db.Float, nullable=False)
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<BankRecords>'

class CreditTables(db.Model):
    __tablename__ = 'credit_tables'
    id = db.Column(db.Integer, primary_key=True)
    card_number = db.Column(db.String(4), nullable=False)
    entries = db.relationship("CreditEntries")
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<CreditTables>'

class CreditEntries(db.Model):
    __tablename__ = 'credit_entries'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(1000), nullable=False)
    price = db.Column(db.Float, nullable=False)
    owner = db.Column(db.Integer, db.ForeignKey('credit_tables.id'))

    def __repr__(self):
        return '<CreditEntries>'