import pandas as pd
import numpy as np
import functools
import datetime

def format_date(d):
    try:
        return datetime.datetime.strptime(d, "%d/%m/%Y")
    except Exception as e:
        return datetime.datetime.strptime(d, "%m/%d/%y")
    

def detect_normal_expenses(data):
    copy_data = list(map(lambda x: [x['amount'], format_date(x['date']) , x['name']], data))
    df = pd.DataFrame(copy_data)
    all_recurring_dests = []
    for dest in set(df[2].values):
        df_filtered = df[df[2] == dest]  # get all the businesses that same as mine
        df_filtered = df_filtered.sort_values(1)  # sort by col 1
        df_filtered[1] = pd.to_datetime(df_filtered[1])
        df_filtered['lag'] = df_filtered[1].shift(-1)  # rise the row by one
        df_filtered['days_diff'] = df_filtered[1] - df_filtered['lag']  # find diff
        if (len(df_filtered) > 2) and (
                len(set(df_filtered['days_diff'].values)) == 2) and (  # check if there is one diff value, +1 because of the null at the end
                np.timedelta64(0, 'ns') not in set(df_filtered['days_diff'].values)):  # the second event is not in the same day
            all_recurring_dests.append(dest)
        elif (len(set(df_filtered[1].dt.day.values)) == 1) and (len(set(df_filtered[1].dt.month.values)) > 1):  # days in months
            all_recurring_dests.append(dest)
    
    return_data = []
    for dst in all_recurring_dests:
        all_data = list(filter(lambda x: x['name'] == dst, data))
        return_data.append(
            {
                'date': None,
                'name': dst,
                'price': functools.reduce(lambda v, e: (v+e['amount'])/2, all_data[1:], all_data[0]['amount']), 
                'occurances': [i['date'] for i in all_data]
            }
        )    

    return return_data
