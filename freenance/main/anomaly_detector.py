import pandas as pd
import numpy as np
from sklearn.ensemble import IsolationForest

def detect_anomalies(data):
    new_data = [a['amount'] for a in data]
    df = pd.DataFrame(new_data)
    isolation_forest = IsolationForest(n_estimators=100)
    isolation_forest.fit(df[0].values.reshape(-1, 1))
    prediction = isolation_forest.predict(np.array(df[0]).reshape(-1, 1))
    return [item for i, item in enumerate(data) if prediction[i] == -1]
