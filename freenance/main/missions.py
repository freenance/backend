import time
from scrapit.main import enrich


def _get_normal_expense_missions(normal_expenses) -> list:
    missions = []
    for expense in normal_expenses:
        p = enrich(expense['name'])
        phone, email, website = p
        missions.append({
            'name': expense['name'],
            "email": email,
            "phone": phone,
            "website": website,
            'occurances': expense['occurances']
        })
    
    return missions

def _get_anomaly_missions(anomalies) -> list:
    missions = []
    for anomaly in anomalies:
        p = None
        while p is None:
            p = enrich(anomaly['name'])
            
        phone, email, website = p
        missions.append({
            'name': anomaly['name'],
            "email": email,
            "phone": phone,
            "website": website,
            'occurances': [anomaly['date']]
        })
    
    return missions

def get_missions(normal_expenses, anomalies):
    return _get_anomaly_missions(anomalies) + _get_normal_expense_missions(normal_expenses)
