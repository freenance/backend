from flask import current_app
from flask_sqlalchemy import SQLAlchemy

from werkzeug.local import LocalProxy

_instance = None
def get_db() -> SQLAlchemy:
    global _instance
    if not _instance:
        _instance = SQLAlchemy(current_app)
    return _instance

db = LocalProxy(get_db) # type: SQLAlchemy
