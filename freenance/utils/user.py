from functools import wraps

from flask import request, jsonify

def logged_in(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        if request.method == 'OPTIONS':
            return "ok", 200
        from freenance.models.user import User
        j = request.get_json()
        user = User.query.filter_by(id=j['id']).first()
        if not user:
            resp = jsonify(success=False)
            resp.status_code = 401
            return resp
        return func(*args, **kwargs, user=user)
    return wrapped